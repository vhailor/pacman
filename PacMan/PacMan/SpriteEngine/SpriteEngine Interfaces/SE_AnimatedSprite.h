//
//  SE_AnimatedSprite.h
//  PacMan
//
//  Created by Victor Sukochev on 30/01/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "SE_Sprite.h"


@protocol SE_AnimatedSprite <SE_Sprite>
@required

- (void) addAnimationFrame:(CGRect)frame;

/*! Animation
 */
- (void) animate;
- (void) pause;
- (void) stop;

@end
