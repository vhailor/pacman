//
//  SE_Scene.h
//  PacMan
//
//  Created by Victor Sukochev on 30/01/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "SE_AnimatedSprite.h"

/*! Scene consists from sprites and animated sprites
 */

@protocol SE_Scene <NSObject>
@required

@property (nonatomic, readonly) NSSet* sprites;

- (void) addSprite:(id <SE_Sprite>)sprite;
- (void) addAnimatedSprite:(id <SE_AnimatedSprite>) animatedSprite;

- (void) removeSprite:(id <SE_Sprite>)sprite;

- (id <SE_Sprite>) defaultSprite;

- (NSArray*) imagesPathsToLoad;
- (NSSet*)   spritesForImageWithPath:(NSString*)imagePath;


@end