//
//  PM_SpriteEngine.h
//  PacMan
//
//  Created by Victor Sukochev on 30/01/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "SE_Scene.h"

/*! Manages sprite scenes. And each sprite scene individually.
 */

@protocol PM_SpriteEngine <NSObject>
@required

- (void) loadScene:(id <SE_Scene>) scene
 withProgressBlock:(void (^)(CGFloat progress)) progressBlock
   completionBlock:(void (^)(NSError*)) completionBlock;

/*! Clearing scene resources
 */
- (void) unloadScene:(id <SE_Scene>) scene;

/*! Only one scene can be presented over time.
 Before presentation scene must be loaded.
 */
- (void) present:(id <SE_Scene>) scene atFrame:(CGRect)frame;

/*! Updating presented scene
 */
- (void) update;

@end
