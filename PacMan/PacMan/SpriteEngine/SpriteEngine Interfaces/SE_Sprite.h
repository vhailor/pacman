//
//  SE_Sprite.h
//  PacMan
//
//  Created by Victor Sukochev on 30/01/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import <Foundation/Foundation.h>

/*! Abstract Sprite Interface
 */

@protocol SE_Sprite <NSObject>
@required

/*! Position to present properties
 */
@property (nonatomic, assign) CGPoint  position;
@property (nonatomic, assign) CGRect   frame;
@property (nonatomic, assign) CGFloat  z;

/*! imagePath acts as unque ID for texture inside engine
 */
@property (nonatomic, strong) NSString* imagePath;

/*! Frame inside image to be used for sprite
 */
@property (nonatomic, assign)  CGRect textureFrame;

/*! Used for textureFrame conversion to relative texture coords
  Should be filled by engine automaticaly
 */
// TODO: Hide texture size inside engine logic
@property (nonatomic, assign)  CGSize textureSize;
           
@end