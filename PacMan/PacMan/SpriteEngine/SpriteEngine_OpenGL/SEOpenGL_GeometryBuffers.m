//
//  SEOpenGL_GeometryBuffers.m
//  PacMan
//
//  Created by Victor Sukochev on 02/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "SEOpenGL_GeometryBuffers.h"

@implementation SEOpenGL_GeometryBuffers

#pragma mark - Lifecycle 

+ (SEOpenGL_GeometryBuffers*) buffersWithSpritesSet:(NSSet*)spritesSet
{
    return [[SEOpenGL_GeometryBuffers alloc] initWithSpritesSet:spritesSet];
}

- (id) initWithSpritesSet:(NSSet*)spritesSet
{
    self = [super init];
    
    if ( self != nil )
    {
        [self generateBuffersFromSpritesSet:spritesSet];
    }
    
    return self;
}

- (void) dealloc
{
    if ( _vertexes != nil )
    {
        free(_vertexes);
    }
}

#pragma mark - Private methods

- (void) generateBuffersFromSpritesSet:(NSSet*)spritesSet
{
    _numberOfVertexes = spritesSet.count * 6;
    _vertexes = malloc(sizeof(SEGeometry_Vertex) * _numberOfVertexes);
    GLuint vertexOffset = 0;
    for ( id <SE_Sprite> sprite in spritesSet )
    {
        CGRect relativeFrame = CGRectMake(sprite.textureFrame.origin.x     / sprite.textureSize.width,
                                           sprite.textureFrame.origin.y    / sprite.textureSize.height,
                                           sprite.textureFrame.size.width  / sprite.textureSize.width,
                                           sprite.textureFrame.size.height / sprite.textureSize.height);
        
        
        _vertexes[vertexOffset].x  = sprite.frame.origin.x;
        _vertexes[vertexOffset].y  = sprite.frame.origin.y;
        _vertexes[vertexOffset].z  = sprite.z;
        _vertexes[vertexOffset].tx = relativeFrame.origin.x;
        _vertexes[vertexOffset].ty = relativeFrame.origin.y;
        
        _vertexes[vertexOffset + 1].x  = sprite.frame.origin.x  + sprite.frame.size.width;
        _vertexes[vertexOffset + 1].y  = sprite.frame.origin.y;
        _vertexes[vertexOffset + 1].tx = relativeFrame.origin.x + relativeFrame.size.width;
        _vertexes[vertexOffset + 1].z  = sprite.z;
        _vertexes[vertexOffset + 1].ty = relativeFrame.origin.y;        
        
        _vertexes[vertexOffset + 2].x  = sprite.frame.origin.x  + sprite.frame.size.width;
        _vertexes[vertexOffset + 2].y  = sprite.frame.origin.y  + sprite.frame.size.height;
        _vertexes[vertexOffset + 2].z  = sprite.z;
        _vertexes[vertexOffset + 2].tx = relativeFrame.origin.x + relativeFrame.size.width;
        _vertexes[vertexOffset + 2].ty = relativeFrame.origin.y + relativeFrame.size.height;
        
        _vertexes[vertexOffset + 3].x  = sprite.frame.origin.x;
        _vertexes[vertexOffset + 3].y  = sprite.frame.origin.y;
        _vertexes[vertexOffset + 3].z  = sprite.z;
        _vertexes[vertexOffset + 3].tx = relativeFrame.origin.x;
        _vertexes[vertexOffset + 3].ty = relativeFrame.origin.y;
        
        _vertexes[vertexOffset + 4].x  = sprite.frame.origin.x + sprite.frame.size.width;
        _vertexes[vertexOffset + 4].y  = sprite.frame.origin.y  + sprite.frame.size.height;
        _vertexes[vertexOffset + 4].z  = sprite.z;
        _vertexes[vertexOffset + 4].tx = relativeFrame.origin.x + relativeFrame.size.width;
        _vertexes[vertexOffset + 4].ty = relativeFrame.origin.y + relativeFrame.size.height;
        
        _vertexes[vertexOffset + 5].x  = sprite.frame.origin.x;
        _vertexes[vertexOffset + 5].y  = sprite.frame.origin.y + sprite.frame.size.height;
        _vertexes[vertexOffset + 5].z  = sprite.z;
        _vertexes[vertexOffset + 5].tx = relativeFrame.origin.x;
        _vertexes[vertexOffset + 5].ty = relativeFrame.origin.y + relativeFrame.size.height;
        
        vertexOffset += 6;
    }
}

@end
