//
//  SEOpenGL_Sprite.h
//  PacMan
//
//  Created by Victor Sukochev on 30/01/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "SE_Sprite.h"

@interface SEOpenGL_Sprite : NSObject <SE_Sprite>

@end
