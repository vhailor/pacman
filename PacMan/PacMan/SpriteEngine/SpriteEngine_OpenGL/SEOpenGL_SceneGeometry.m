//
//  SEOpenGL_SceneGeometry.m
//  PacMan
//
//  Created by Victor Sukochev on 02/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "SEOpenGL_SceneGeometry.h"

@interface SEOpenGL_SceneGeometry()

- (void) generateGeometryForScene:(id <SE_Scene> )scene;

@end

@implementation SEOpenGL_SceneGeometry
{
    @private
    id <SE_Scene> _scene;
    NSMutableDictionary* _geometryBuffersDict;
}

#pragma mark - Lifecycle 

+ (SEOpenGL_SceneGeometry*) geometryWithScene:(id <SE_Scene>)scene
{
    return [[SEOpenGL_SceneGeometry alloc] initWithScene:scene];
}

- (id) initWithScene:(id <SE_Scene>)scene
{
    self = [super init];
    
    if ( self != nil )
    {        
        _geometryBuffersDict = [NSMutableDictionary dictionary];
        [self generateGeometryForScene:scene];
    }
    
    return self;
}

#pragma mark - Public methods

- (SEOpenGL_GeometryBuffers*) buffersForTextureWithID:(NSString*)textureID
{
    return _geometryBuffersDict[textureID];
}

#pragma mark - Private methods

- (void) generateGeometryForScene:(id <SE_Scene> )scene
{
    for ( NSString* imagePath in [scene imagesPathsToLoad] )
    {
        NSSet* spritesSet = [scene spritesForImageWithPath:imagePath];
        [_geometryBuffersDict setObject:[SEOpenGL_GeometryBuffers buffersWithSpritesSet:spritesSet]
                                 forKey:imagePath];
    }
}

@end
