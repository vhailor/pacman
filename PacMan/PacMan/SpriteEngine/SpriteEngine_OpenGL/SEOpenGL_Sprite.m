//
//  SEOpenGL_Sprite.m
//  PacMan
//
//  Created by Victor Sukochev on 30/01/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "SEOpenGL_Sprite.h"

@interface SEOpenGL_Sprite()
{
    @private
    CGPoint _position;
    CGRect  _frame;
}

@end

@implementation SEOpenGL_Sprite


#pragma mark - Implementation of SE_Sprite interface methods

@synthesize position = _position;
@synthesize frame = _frame;
@synthesize z;
@synthesize imagePath;
@synthesize textureFrame;
@synthesize textureSize;

- (void) setFrame:(CGRect)frame
{
    _frame = frame;
    
    _position = CGPointMake(_frame.origin.x + _frame.size.width  / 2.0f,
                            _frame.origin.y + _frame.size.height / 2.0f);
}

- (void) setPosition:(CGPoint)position
{
    _position = position;
    
    _frame = CGRectMake(_position.x - _frame.size.width  / 2.0f,
                        _position.y - _frame.size.height / 2.0f,
                        _frame.size.width,
                        _frame.size.height);
}

@end
