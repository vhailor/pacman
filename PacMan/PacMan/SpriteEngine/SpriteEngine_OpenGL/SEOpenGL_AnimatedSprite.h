//
//  SEOpenGL_AnimatedSprite.h
//  PacMan
//
//  Created by Victor Sukochev on 30/01/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "SEOpenGL_Sprite.h"
#import "SE_AnimatedSprite.h"

@interface SEOpenGL_AnimatedSprite : SEOpenGL_Sprite <SE_AnimatedSprite>

@end
