//
//  PM_SpriteEngineOpenGL.h
//  PacMan
//
//  Created by Victor Sukochev on 30/01/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import <GLKit/GLKit.h>

#import "PM_SpriteEngine.h"

@interface PM_SpriteEngineOpenGL : NSObject <PM_SpriteEngine>

@property (nonatomic, strong) EAGLContext* context;

- (void) render;

@end
