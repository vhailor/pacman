//
//  SEOpenGL_Scene.h
//  PacMan
//
//  Created by Victor Sukochev on 30/01/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "SE_Scene.h"

@interface SEOpenGL_Scene : NSObject <SE_Scene>

@end
