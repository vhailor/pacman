//
//  SEOpenGL_AnimatedSprite.m
//  PacMan
//
//  Created by Victor Sukochev on 30/01/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "SEOpenGL_AnimatedSprite.h"

@implementation SEOpenGL_AnimatedSprite

#pragma mark - SE_AnimatedSprite interface methods

- (void) addAnimationFrame:(CGRect)frame
{
    
}

- (void) animate
{
    
}

- (void) pause
{
    
}

- (void) stop
{
    
}

@end
