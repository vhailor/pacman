//
//  SEOpenGL_TextureInfo.h
//  PacMan
//
//  Created by Victor Sukochev on 04/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SEOpenGL_TextureInfo : NSObject

@property (nonatomic, assign) uint   textureID;
@property (nonatomic, assign) CGSize size;

@end
