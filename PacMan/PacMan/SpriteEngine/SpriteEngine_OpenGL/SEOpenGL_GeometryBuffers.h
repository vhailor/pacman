//
//  SEOpenGL_GeometryBuffers.h
//  PacMan
//
//  Created by Victor Sukochev on 02/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import <GLKit/GLKit.h>

#import "SE_Sprite.h"

typedef struct
{
    GLfloat x;
    GLfloat y;
    GLfloat z;
    GLfloat tx;
    GLfloat ty;
    
} SEGeometry_Vertex;

@interface SEOpenGL_GeometryBuffers : NSObject

+ (SEOpenGL_GeometryBuffers*) buffersWithSpritesSet:(NSSet*)spritesSet;

- (id) initWithSpritesSet:(NSSet*)spritesSet;

@property (nonatomic, readonly) SEGeometry_Vertex* vertexes;
@property (nonatomic, readonly) GLuint numberOfVertexes;

@end
