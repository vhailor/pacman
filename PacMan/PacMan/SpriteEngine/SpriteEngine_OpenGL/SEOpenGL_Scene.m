//
//  SEOpenGL_Scene.m
//  PacMan
//
//  Created by Victor Sukochev on 30/01/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "SEOpenGL_Scene.h"
#import "SEOpenGL_Sprite.h"

@implementation SEOpenGL_Scene
{
    @private
    NSMutableDictionary* _resourceSpritesDict;
    NSMutableSet*        _sprites;
    NSMutableSet*        _animatedSprites;
}

@synthesize sprites = _sprites;

#pragma mark - Lifecycle

- (id) init
{
    self = [super init];
    if ( self != nil )
    {
        _resourceSpritesDict = [NSMutableDictionary dictionary];
        _sprites             = [NSMutableSet set];
        _animatedSprites     = [NSMutableSet set];
    }
    return self;
}

#pragma mark - SE_Scene interface methods

- (void) addSprite:(id <SE_Sprite>)sprite
{
    [_sprites addObject:sprite];
    [self addSpriteToResourceDict:sprite];
}

- (void) addAnimatedSprite:(id <SE_AnimatedSprite>) animatedSprite
{
    [_animatedSprites addObject:animatedSprite];
    [self addSpriteToResourceDict:animatedSprite];
}

- (void) removeSprite:(id <SE_Sprite>)sprite
{
    [_sprites removeObject:sprite];
    [self removeSpriteFromResourceDict:sprite];
}

- (id <SE_Sprite>) defaultSprite
{
    return [SEOpenGL_Sprite new];
}

- (NSArray*) imagesPathsToLoad
{
    return _resourceSpritesDict.allKeys;
}

- (NSSet*) spritesForImageWithPath:(NSString*)imagePath
{
    return _resourceSpritesDict[imagePath];
}

#pragma mark - Private methods

/*! Orginizing all sprites inside dictionary
 to load unique textures later
*/
- (void) addSpriteToResourceDict:(id <SE_Sprite>)sprite
{
    NSString* resourceKey = sprite.imagePath;
    if ( resourceKey == nil)
    {
        return;
    }
    
    if ( [_resourceSpritesDict objectForKey:resourceKey] == nil )
    {
        [_resourceSpritesDict setObject:[NSMutableSet set] forKey:resourceKey];
    }
    
    NSMutableSet* spritesForResourceKey = [_resourceSpritesDict objectForKey:resourceKey];
    [spritesForResourceKey addObject:sprite];
}

- (void) removeSpriteFromResourceDict:(id <SE_Sprite>)sprite
{
    NSString* resourceKey = sprite.imagePath;
    if ( resourceKey == nil)
    {
        return;
    }
    
    if ( [_resourceSpritesDict objectForKey:resourceKey] == nil )
    {
        return;
    }
    
    NSMutableSet* spritesForResourceKey = [_resourceSpritesDict objectForKey:resourceKey];
    [spritesForResourceKey removeObject:sprite];
}

@end
