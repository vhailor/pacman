//
//  PM_SpriteEngineOpenGL.m
//  PacMan
//
//  Created by Victor Sukochev on 30/01/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "PM_SpriteEngineOpenGL.h"

#import "SEOpenGL_SceneGeometry.h"
#import "SEOpenGL_TextureInfo.h"

//#define BUFFER_OFFSET(i) ((char *)NULL + (i))

@interface PM_SpriteEngineOpenGL()

- (void)      updateGeometryForScene:(id <SE_Scene>)scene;
- (NSString*) uniqueKeyForScene:     (id <SE_Scene>)scene;

@end

@implementation PM_SpriteEngineOpenGL
{
    @private
    NSMutableDictionary* _textureIdForImageKeyDict;
    NSMutableDictionary* _geometryForSceneDict;
    GLKBaseEffect* _effect;
    GLuint _currentTextureID;
    
    id <SE_Scene> _presentedScene;
}

#pragma mark - Lifecycle

- (id) init
{
    self = [super init];
    
    if ( self != nil )
    {
        _textureIdForImageKeyDict = [NSMutableDictionary dictionary];
        _geometryForSceneDict     = [NSMutableDictionary dictionary];
    }
    
    return self;
}

#pragma mark - PM_SpriteEngine interface methods

- (void) loadScene:(id <SE_Scene>) scene
 withProgressBlock:(void (^)(CGFloat progress)) progressBlock
   completionBlock:(void (^)(NSError*)) completionBlock;
{    
    //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        // Loading textures
        NSArray* imagesToLoad = [scene imagesPathsToLoad];
        float progressStep = 1.0f / imagesToLoad.count;
        float progress     = 0.0f;
        NSError* error     = nil;
        
        for ( NSString* imagePath in imagesToLoad )
        {
            error = [self loadImageWithPath:imagePath];
            if ( error != nil )
            {
                break;
            }
            
            progress += progressStep;
            progressBlock(progress);
        }
        
        //Setting up textures size
        for ( NSString* imagePath in _textureIdForImageKeyDict.allKeys)
        {
            SEOpenGL_TextureInfo* textureInfo = _textureIdForImageKeyDict[imagePath];
            for ( id <SE_Sprite> sprite in [scene spritesForImageWithPath:imagePath] )
            {
                sprite.textureSize = textureInfo.size;
            }
        }
       
        // Generating geometry
        [_geometryForSceneDict setObject:[SEOpenGL_SceneGeometry geometryWithScene:scene]
                                  forKey:[self uniqueKeyForScene:scene]];
        
        
        completionBlock(error);
    //});
}

- (void) unloadScene:(id <SE_Scene>) scene
{
    for ( NSString* imagePath in [scene imagesPathsToLoad] )
    {
        GLuint textureId = _textureIdForImageKeyDict[imagePath];
        glDeleteTextures(1, &textureId);
        [_textureIdForImageKeyDict removeObjectForKey:imagePath];
    }
}

- (void) present:(id <SE_Scene>) scene atFrame:(CGRect)frame;
{
    _effect = [GLKBaseEffect new];
    
    GLKMatrix4  projectionMatrix = GLKMatrix4MakeOrtho(0, frame.size.width , frame.size.height, 0, 1, -1);    
    _effect.transform.projectionMatrix = projectionMatrix;    
    _presentedScene = scene;
}

- (void) update
{    
    
}

#pragma mark - Public methods

- (void) render
{
    [self updateGeometryForScene:_presentedScene];
    
    //glEnable(GL_TEXTURE_2D);
    SEOpenGL_SceneGeometry* geometry = _geometryForSceneDict[[self uniqueKeyForScene:_presentedScene]];
    for ( NSString* textureID in [_presentedScene imagesPathsToLoad] )
    {
        SEOpenGL_GeometryBuffers* buffers     = [geometry buffersForTextureWithID:textureID];
        SEGeometry_Vertex*        vertexes    = buffers.vertexes;
        SEOpenGL_TextureInfo*     textureInfo = _textureIdForImageKeyDict[textureID];

        _effect.texture2d0.enabled = YES;
        _effect.texture2d0.name = textureInfo.textureID;

        
        
        [_effect prepareToDraw];
        
        glEnableVertexAttribArray(GLKVertexAttribPosition);
        glVertexAttribPointer(GLKVertexAttribPosition, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), &vertexes[0].x);
        
        glEnableVertexAttribArray(GLKVertexAttribTexCoord0);
        glVertexAttribPointer(GLKVertexAttribTexCoord0, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), &vertexes[0].tx);
        
        
        glDrawArrays(GL_TRIANGLES, 0, buffers.numberOfVertexes);
        
        glDisableVertexAttribArray(GLKVertexAttribPosition);
        glDisableVertexAttribArray(GLKVertexAttribTexCoord0);
    }
}

#pragma mark - Private methods

- (NSError*)loadImageWithPath:(NSString*)imagePath
{    
     NSError* error = nil;
    
//    GLKTextureLoader* textureLoader  = [[GLKTextureLoader alloc] initWithSharegroup:_context.sharegroup];
//    [textureLoader textureWithContentsOfFile:imagePath
//                                     options:nil
//                                       queue:dispatch_get_current_queue()
//                           completionHandler:^(GLKTextureInfo *textureInfo, NSError *outError) {
//                               error = outError;
//                               SEOpenGL_TextureInfo* texInfo = [SEOpenGL_TextureInfo new];
//                               texInfo.textureID   = textureInfo.name;
//                               texInfo.size = CGSizeMake(textureInfo.width, textureInfo.height);
//                               [_textureIdForImageKeyDict setObject:texInfo forKey:imagePath];
//                           }];
    
    GLKTextureInfo* textureInfo = [GLKTextureLoader textureWithContentsOfFile:imagePath
                                                                      options:nil
                                                                        error:&error];
    
    SEOpenGL_TextureInfo* texInfo = [SEOpenGL_TextureInfo new];
    texInfo.textureID   = textureInfo.name;
    texInfo.size = CGSizeMake(textureInfo.width, textureInfo.height);
    [_textureIdForImageKeyDict setObject:texInfo forKey:imagePath];

    return error;
}
- (void) updateGeometryForScene:(id <SE_Scene>)scene
{
    [_geometryForSceneDict setObject:[SEOpenGL_SceneGeometry geometryWithScene:scene]
                              forKey:[self uniqueKeyForScene:scene]];
}

- (NSString*)uniqueKeyForScene:(id <SE_Scene>)scene
{
    return [NSString stringWithFormat:@"%d", [scene hash]];
}

@end
