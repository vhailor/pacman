//
//  SEOpenGL_SceneGeometry.h
//  PacMan
//
//  Created by Victor Sukochev on 02/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "SEOpenGL_GeometryBuffers.h"
#import "SE_Scene.h"


@interface SEOpenGL_SceneGeometry : NSObject

+ (SEOpenGL_SceneGeometry*) geometryWithScene:(id <SE_Scene>)scene;

- (id) initWithScene:(id <SE_Scene>)scene;

- (SEOpenGL_GeometryBuffers*) buffersForTextureWithID:(NSString*)textureID;

@end
