//
//  PM_InputView.m
//  PacMan
//
//  Created by Victor Sukochev on 16/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "PM_InputView.h"

const CGFloat  kMinimumDetectionTreshhold = 20.0f;

@implementation PM_InputView
{
    @private
    MovementDirection _movementDirection;
    CGPoint _lastLocation;
}

//- (id)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//        // Initialization code
//    }
//    return self;
//}

#pragma mark - Lifecycle

- (void) awakeFromNib
{
    //TEST: 
    UIPanGestureRecognizer* recognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
    [self addGestureRecognizer:recognizer];
}

#pragma mark - PM_DirectionInput methods

- (MovementDirection) getCurrentDirection
{
    return _movementDirection;
}

- (void) reset
{
    _movementDirection = MovementDirectionUnknown;
}

#pragma mark - Gesture recognizer handlers

- (void) handlePanGesture:(UIPanGestureRecognizer*)recognizer
{
    CGPoint location = [recognizer locationInView:self];
    
    if ( recognizer.state == UIGestureRecognizerStateBegan )
    {
        _lastLocation = location;
    }
    
    if ( recognizer.state == UIGestureRecognizerStateChanged )
    {
        CGPoint movingVector = CGPointMake(location.x - _lastLocation.x,
                                           location.y - _lastLocation.y);
        
        if ( sqrtf(movingVector.x * movingVector.x + movingVector.y * movingVector.y) < kMinimumDetectionTreshhold )
        {
            return;
        }
        
        if ( fabs(movingVector.x) >=  fabs(movingVector.y) )
        {
            // Horizontal gesture
            _movementDirection = (movingVector.x >= 0) ? MovementDirectionRight : MovementDirectionLeft;
        }
        else
        {
            // Vertical gesture
            _movementDirection = (movingVector.y >= 0) ? MovementDirectionDown : MovementDirectionUp;
        }
        
        _lastLocation = location;
    }
    
//    if ( recognizer.state == UIGestureRecognizerStateEnded )
//    {
//        _movementDirection = MovementDirectionUnknown;
//    }
    

}


@end
