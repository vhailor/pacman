//
//  PM_InputView.h
//  PacMan
//
//  Created by Victor Sukochev on 16/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PM_DirectionInput.h"

@interface PM_InputView : UIView <PM_DirectionInput>

@end
