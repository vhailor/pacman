//
//  PM_AppDelegate.m
//  PacMan
//
//  Created by Victor Sukochev on 29/01/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "PM_AppDelegate.h"

#import "PM_SpriteEngineOpenGLViewController.h"
//#import "SEOpenGL_Scene.h"

@interface PM_AppDelegate()

- (void) initializeEngine;

@end

@implementation PM_AppDelegate

#pragma mark - Lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        self.viewController = [[PM_SpriteEngineOpenGLViewController alloc] initWithNibName:@"PM_SpriteEngineOpenGLViewController_iPhone" bundle:nil];
    } else {
        self.viewController = [[PM_SpriteEngineOpenGLViewController alloc] initWithNibName:@"PM_SpriteEngineViewController_iPad" bundle:nil];
    }
    
    [self initializeEngine];
    
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Private methods

- (void) initializeEngine
{
    PM_SpriteEngineOpenGL* engine  = [PM_SpriteEngineOpenGL new];
    _viewController.engine = engine;    
}

@end
