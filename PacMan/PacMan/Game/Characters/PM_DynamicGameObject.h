//
//  PM_DynamicGameObject.h
//  PacMan
//
//  Created by Victor Sukochev on 11/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PM_FieldObject.h"

@interface PM_DynamicGameObject : PM_FieldObject

@property (nonatomic, assign) CGRect fieldFrame;

- (void) update:(NSTimeInterval)dT
  staticObjects:(NSArray*)staticObjects
      fieldSize:(CGSize)fieldSize
     fieldFrame:(CGRect)frame
         pacman:(PM_FieldObject*)pacman;

@end
