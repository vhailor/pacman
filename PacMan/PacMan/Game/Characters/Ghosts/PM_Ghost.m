//
//  PM_Ghost.m
//  PacMan
//
//  Created by Victor Sukochev on 11/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "PM_Ghost.h"

#import "PM_StaticGameObject.h"
#import "PM_DirectionInput.h"

const CGFloat kGhostDefaultSpeed = 30.0f;

@implementation PM_Ghost
{
@private
    CGFloat                _speed;
    CGSize                 _fieldSize;
    CGRect                 _fieldFrame;
    NSArray*               _staticObjects;
    PM_FieldObject*        _pacman;
    NSTimeInterval         _dT;
    MovementDirection      _previousDirection;
}


- (id) init
{
    self = [super init];
    
    if ( self != nil )
    {
        _speed = kGhostDefaultSpeed;
    }
    
    return self;
}

- (void) update:(NSTimeInterval)dT
  staticObjects:(NSArray*)staticObjects
      fieldSize:(CGSize)fieldSize
     fieldFrame:(CGRect)frame
         pacman:(PM_FieldObject*)pacman
{
    _fieldSize     = fieldSize;
    _fieldFrame   = frame;
    _staticObjects = staticObjects;
    _pacman        = pacman;
    _dT            = dT;
    
    
    self.fieldPosition =  CGPointMake((uint)(self.sprite.position.x / (frame.size.width  / fieldSize.width)),
                                      (uint)(self.sprite.position.y / (frame.size.height / fieldSize.height)));
    
    
    MovementDirection directionWithShortestDistance = MovementDirectionUnknown;
    CGFloat shortestDistance   = _fieldSize.width * _fieldSize.width + _fieldSize.height * _fieldSize.height;
    CGFloat currentDistance    = 0.0f;
    NSSet*  availableDirection = [self avaibleDirections];
    
    for ( NSNumber* direction in  availableDirection)
    {
        currentDistance = [self distanceForDirection:[direction intValue]];
        
        if ( currentDistance < shortestDistance )
        {
            shortestDistance = currentDistance;
            directionWithShortestDistance = [direction intValue];
        }
    }
    
    [self moveToDirection:directionWithShortestDistance];
    _previousDirection = directionWithShortestDistance;
}

- (NSSet*) avaibleDirections
{
    NSMutableSet* availableDirections = [NSMutableSet set];
    
    NSUInteger positionOffset = self.fieldPosition.x + (self.fieldPosition.y - 1) * _fieldSize.width;    
    if ( self.fieldPosition.y > 0 &&
        ((PM_StaticGameObject*)_staticObjects[positionOffset]).isPassable &&
        _previousDirection != MovementDirectionDown)
    {
        [availableDirections addObject:@(MovementDirectionUp)];
    }
    
    positionOffset = self.fieldPosition.x + (self.fieldPosition.y + 1) * _fieldSize.width;
    if ( self.fieldPosition.y < _fieldSize.height &&
        ((PM_StaticGameObject*)_staticObjects[positionOffset]).isPassable  &&
        _previousDirection != MovementDirectionUp)
    {
        [availableDirections addObject:@(MovementDirectionDown)];
    }
    
    positionOffset = self.fieldPosition.x + 1 + self.fieldPosition.y * _fieldSize.width;    
    if ( self.fieldPosition.x < _fieldSize.width &&
        ((PM_StaticGameObject*)_staticObjects[positionOffset]).isPassable  &&
        _previousDirection != MovementDirectionLeft)
    {
        [availableDirections addObject:@(MovementDirectionRight)];
    }
  
    positionOffset = self.fieldPosition.x - 1 + self.fieldPosition.y * _fieldSize.width;
    if ( self.fieldPosition.x > 0 &&
        ((PM_StaticGameObject*)_staticObjects[positionOffset]).isPassable  &&
        _previousDirection != MovementDirectionRight)
    {
        [availableDirections addObject:@(MovementDirectionLeft)];
    }
    
    return availableDirections;
}

- (CGFloat) distanceForDirection:(MovementDirection)direction
{
    switch (direction)
    {
        case MovementDirectionUnknown:
            NSAssert(0, @"Wrong direction");
            return 0.0f;
            break;
            
        case MovementDirectionUp:
            return powf((_pacman.fieldPosition.x -  self.fieldPosition.x), 2) +
                   powf((_pacman.fieldPosition.y - (self.fieldPosition.y - 1)), 2);
            break;
            
        case MovementDirectionDown:
            return powf((_pacman.fieldPosition.x -  self.fieldPosition.x), 2) +
                   powf((_pacman.fieldPosition.y - (self.fieldPosition.y + 1)), 2);
            break;
            
        case MovementDirectionLeft:
            return powf((_pacman.fieldPosition.x - (self.fieldPosition.x - 1)), 2) +
                   powf((_pacman.fieldPosition.y -  self.fieldPosition.y), 2);
            break;
            
        case MovementDirectionRight:
            return powf((_pacman.fieldPosition.x - (self.fieldPosition.x + 1)), 2) +
                   powf((_pacman.fieldPosition.y -  self.fieldPosition.y), 2);
            break;
            
        default:
            NSAssert(0, @"Wrong direction");
            break;
    }
    
    return 0.0f;
}

- (void) moveToDirection:(MovementDirection)direction
{
    
    CGFloat dx      = (_fieldFrame.size.width / _fieldSize.width);
    CGFloat dy      = (_fieldFrame.size.height / _fieldSize.height);
    CGFloat hWidth  = self.sprite.frame.size.width / 2.0f;
    CGFloat hHeight = self.sprite.frame.size.height / 2.0f;
    
    switch (direction)
    {
        case MovementDirectionUnknown:
            //Do nothing
            break;
            
        case MovementDirectionUp:
            self.sprite.position = CGPointMake(self.fieldPosition.x * dx + hWidth,
                                               self.sprite.position.y - (_speed * _dT));
            break;
            
        case MovementDirectionDown:
            self.sprite.position = CGPointMake(self.fieldPosition.x * dx + hWidth,
                                               self.sprite.position.y + (_speed * _dT));
            break;
            
        case MovementDirectionLeft:
            self.sprite.position = CGPointMake(self.sprite.position.x - (_speed * _dT),
                                               self.fieldPosition.y * dy + hHeight);
            break;
            
        case MovementDirectionRight:
            self.sprite.position = CGPointMake(self.sprite.position.x + (_speed * _dT),
                                               self.fieldPosition.y * dy +hHeight);
            break;
            
        default:
            NSAssert(0, @"Wrong direction");
            break;
    }
    
    self.fieldPosition =  CGPointMake((uint)(self.sprite.position.x / (_fieldFrame.size.width  / _fieldSize.width)),
                                      (uint)(self.sprite.position.y / (_fieldFrame.size.height / _fieldSize.height)));
}

// Deprecated
- (PM_StaticGameObject*) getUnpassableObjectAtPosition:(CGPoint)position staticObjects:(NSArray*)staticObjects
{
    CGFloat halfWidth  = self.sprite.frame.size.width  / 2.0f;
    CGFloat halfHeight = self.sprite.frame.size.height / 2.0f;

    CGPoint topLeftCorner     = CGPointMake(position.x - halfWidth,
                                            position.y - halfHeight );
    CGPoint topRightCorner    = CGPointMake(position.x + halfWidth,
                                            position.y - halfHeight );
    CGPoint bottomLeftCorner  = CGPointMake(position.x - halfWidth,
                                            position.y + halfHeight );
    CGPoint bottomRightCorner = CGPointMake(position.x + halfWidth,
                                            position.y + halfHeight );
    for ( PM_StaticGameObject* staticObject in staticObjects )
    {
        if ( CGRectContainsPoint(staticObject.sprite.frame, topLeftCorner) && !staticObject.isPassable )
        {
            return staticObject;
        }
        
        if ( CGRectContainsPoint(staticObject.sprite.frame, topRightCorner) && !staticObject.isPassable )
        {
            return staticObject;
        }
        
        if ( CGRectContainsPoint(staticObject.sprite.frame, bottomLeftCorner) && !staticObject.isPassable )
        {
            return staticObject;
        }
        
        if ( CGRectContainsPoint(staticObject.sprite.frame, bottomRightCorner) && !staticObject.isPassable )
        {
            return staticObject;
        }
    }
    
    return nil;
}

@end
