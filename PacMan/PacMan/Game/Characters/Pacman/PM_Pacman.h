//
//  PM_Pacman.h
//  PacMan
//
//  Created by Victor Sukochev on 11/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "PM_DynamicGameObject.h"

#import "PM_DirectionInput.h"

@interface PM_Pacman : PM_DynamicGameObject

@property (nonatomic, strong) id <PM_DirectionInput> directionInput;


@end
