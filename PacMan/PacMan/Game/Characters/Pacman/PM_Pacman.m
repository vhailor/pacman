//
//  PM_Pacman.m
//  PacMan
//
//  Created by Victor Sukochev on 11/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "PM_Pacman.h"

#import "PM_StaticGameObject.h"

const CGFloat kDefaultPacmanSpeed = 45;

@implementation PM_Pacman
{
    @private
    NSArray*       _staticObjects;
    CGSize         _fieldSize;
    CGRect         _fieldFrame;
    NSTimeInterval _dT;
}

- (void) update:(NSTimeInterval)dT
  staticObjects:(NSArray *)staticObjects
      fieldSize:(CGSize)fieldSize
     fieldFrame:(CGRect)frame
         pacman:(PM_FieldObject *)pacman
{
    _staticObjects = staticObjects;
    _fieldSize     = fieldSize;
    _fieldFrame    = frame;
    _dT            = dT;
    
    
    self.fieldPosition =  CGPointMake((uint)((self.sprite.position.x) / (frame.size.width  / fieldSize.width)),
                                      (uint)((self.sprite.position.y) / (frame.size.height / fieldSize.height)));
    
 
    
    MovementDirection direction = [_directionInput getCurrentDirection];
    
    if ( ![self isValidDirection:direction] )
    {
        // Fast turning fix
        //TODO: turning
        CGFloat dx      = (_fieldFrame.size.width / _fieldSize.width);
        CGFloat dy      = (_fieldFrame.size.height / _fieldSize.height);
        CGFloat hWidth  = self.sprite.frame.size.width / 2.0f;
        CGFloat hHeight = self.sprite.frame.size.height / 2.0f;
        
        self.sprite.position = CGPointMake(self.fieldPosition.x * dx + hWidth,
                                           self.fieldPosition.y * dy + hHeight);
        return;
    }
    
    [self moveUsingDirection:direction];
}

- (BOOL) isValidDirection:(MovementDirection)direction
{
    NSUInteger positionOffset = self.fieldPosition.x + self.fieldPosition.y  * _fieldSize.width;
    switch (direction)
    {
        case MovementDirectionUnknown:
            return NO;
            break;
            
        case MovementDirectionUp:
            if (self.fieldPosition.y > 0)
            {
                positionOffset -= _fieldSize.width;
            }
            
            break;
            
        case MovementDirectionDown:
            if (self.fieldPosition.y < _fieldSize.height)
            {
                positionOffset += _fieldSize.width;
            }
            
            break;
            
        case MovementDirectionLeft:
            if (self.fieldPosition.x > 0)
            {
                positionOffset--;
            }
            
            break;
            
        case MovementDirectionRight:
            if (self.fieldPosition.x < _fieldSize.width)
            {
                positionOffset++;
            }
            
            break;
            
        default:
            NSAssert(0, @"Wrong direction");
            return NO;
            break;
    }
    
    return ((PM_StaticGameObject*)_staticObjects[positionOffset]).isPassable;
}

- (void) moveUsingDirection:(MovementDirection)direction
{
    CGFloat dx      = (_fieldFrame.size.width / _fieldSize.width);
    CGFloat dy      = (_fieldFrame.size.height / _fieldSize.height);
    CGFloat hWidth  = self.sprite.frame.size.width / 2.0f;
    CGFloat hHeight = self.sprite.frame.size.height / 2.0f;
    
    switch (direction)
    {
        case MovementDirectionUnknown:
            //Do nothing
            return;
            break;
            
        case MovementDirectionUp:
            self.sprite.position = CGPointMake(self.fieldPosition.x * dx + hWidth,
                                               self.sprite.position.y - (kDefaultPacmanSpeed * _dT));
            break;
            
        case MovementDirectionDown:
            self.sprite.position = CGPointMake(self.fieldPosition.x * dx + hWidth,
                                               self.sprite.position.y + (kDefaultPacmanSpeed * _dT));
            break;
            
        case MovementDirectionLeft:
            self.sprite.position = CGPointMake(self.sprite.position.x - (kDefaultPacmanSpeed * _dT),
                                               self.fieldPosition.y * dy + hHeight);
            break;
            
        case MovementDirectionRight:
            self.sprite.position = CGPointMake(self.sprite.position.x + (kDefaultPacmanSpeed * _dT),
                                               self.fieldPosition.y * dy + hHeight);
            break;
            
        default:
            NSAssert(0, @"Wrong direction");
            break;

    }
    
    //Updating pacman field position
    
    self.fieldPosition =  CGPointMake((uint)(self.sprite.position.x / (_fieldFrame.size.width  / _fieldSize.width)),
                                      (uint)(self.sprite.position.y / (_fieldFrame.size.height / _fieldSize.height)));
}

@end
