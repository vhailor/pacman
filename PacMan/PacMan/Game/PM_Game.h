//
//  PM_Game.h
//  PacMan
//
//  Created by Victor Sukochev on 12/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PM_Level.h"
#import "PM_DirectionInput.h"

@class PM_Game;

@protocol PM_GameDelegate <NSObject>
@required

- (void) gameDidLost:(PM_Game*)game;
- (void) gameDidWin:(PM_Game*)game;
- (void) game:(PM_Game*)game didPointsLefyUpdate:(NSUInteger)pointsLeft;

@end

@interface PM_Game : NSObject

@property (nonatomic, weak)   id <PM_GameDelegate> gameDelegate;
@property (nonatomic, strong) PM_Level* activeLevel;
@property (nonatomic, strong) id <PM_DirectionInput> directionInput;

- (void) update:(NSTimeInterval)dT;

@end
