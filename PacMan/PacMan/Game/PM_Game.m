//
//  PM_Game.m
//  PacMan
//
//  Created by Victor Sukochev on 12/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "PM_Game.h"

#import "PM_StaticGameObject.h"
#import "PM_PointToEat.h"
#import "PM_Emptiness.h"
#import "PM_Ghost.h"

@implementation PM_Game
{
    @private
    BOOL _didFinish;
}

#pragma mark - Public properties

- (void) setDirectionInput:(id<PM_DirectionInput>)directionInput
{
    _directionInput = directionInput;
    
    _activeLevel.field.pacman.directionInput = directionInput;
}

- (void) setActiveLevel:(PM_Level *)activeLevel
{
    _activeLevel = activeLevel;
    
    _activeLevel.field.pacman.directionInput = _directionInput;
    _didFinish = NO;
}

#pragma mark - Public methods

- (void) update:(NSTimeInterval)dT;
{
    if ( _didFinish )
    {
        return;
    }
    
    for ( PM_DynamicGameObject* dynamicObject in _activeLevel.field.dynamicObjects )
    {
        [dynamicObject update:dT
                staticObjects:_activeLevel.field.staticObjects
                    fieldSize:_activeLevel.field.fieldSize
                   fieldFrame:CGRectMake(0.0f, 0.0f, _activeLevel.field.frameSize.width, _activeLevel.field.frameSize.height)
                       pacman:_activeLevel.field.pacman];

    }
    
    [self checkConditions];
}

#pragma mark - Private methods

- (void) checkConditions
{
    [self checkEatPointCondition];
    [self checkWinCondition];
    [self checkLostCondition];
}

- (void) checkWinCondition
{
    if ( _activeLevel.field.pointsToEatLeft == 0 )
    {
        [_gameDelegate gameDidWin:self];
        
         _didFinish = YES;
    }
}

- (void) checkLostCondition
{
     PM_Pacman* pacman = _activeLevel.field.pacman;
    
    for ( PM_DynamicGameObject* dynamicObject in _activeLevel.field.dynamicObjects )
    {
        if ( [dynamicObject isKindOfClass:[PM_Ghost class]] &&
            CGPointEqualToPoint(dynamicObject.fieldPosition, pacman.fieldPosition))
        {            
            [_gameDelegate gameDidLost:self];

            _didFinish = YES;
        }
    }
}

- (void) checkEatPointCondition
{
    PM_Pacman* pacman             = _activeLevel.field.pacman;
    CGSize fieldSize              = _activeLevel.field.fieldSize;
    NSMutableArray* staticObjects = _activeLevel.field.staticObjects;
    
    PM_StaticGameObject* currentObject = staticObjects[(uint)(pacman.fieldPosition.x + pacman.fieldPosition.y * fieldSize.width)];
    
    if ( [currentObject isKindOfClass:[PM_PointToEat class]] ) // Dont really like runtime, just to make coding faster
    {
        PM_Emptiness* emptinessObject = [PM_Emptiness new];
        emptinessObject.fieldPosition = currentObject.fieldPosition;
        NSUInteger currentIndex = [staticObjects indexOfObject:currentObject];
        
        //TODO: need to incapsulate it insude PM_Field implementation
        [_activeLevel.field.staticObjects replaceObjectAtIndex:currentIndex withObject:emptinessObject];
        [_activeLevel.scene removeSprite:currentObject.sprite];
        
        _activeLevel.field.pointsToEatLeft--;
        [_gameDelegate game:self didPointsLefyUpdate:_activeLevel.field.pointsToEatLeft];
    }
}

@end
