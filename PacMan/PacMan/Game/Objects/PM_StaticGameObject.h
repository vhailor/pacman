//
//  PM_StaticGameObject.h
//  PacMan
//
//  Created by Victor Sukochev on 11/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PM_FieldObject.h"


@interface PM_StaticGameObject : PM_FieldObject

@property (nonatomic, readonly) BOOL isPassable;

@end
