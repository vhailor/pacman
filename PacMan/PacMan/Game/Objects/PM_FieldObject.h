//
//  PM_FieldObject.h
//  PacMan
//
//  Created by Victor Sukochev on 12/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SE_Sprite.h"

@interface PM_FieldObject : NSObject

@property (nonatomic, strong) id <SE_Sprite> sprite;
@property (nonatomic, assign) CGPoint fieldPosition;

@end
