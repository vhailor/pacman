//
//  PM_Level.h
//  PacMan
//
//  Created by Victor Sukochev on 06/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SE_Scene.h"

#import "PM_Field.h"

@interface PM_Level : NSObject

/*! Scene for containing sprites
 */
@property (nonatomic, strong) id <SE_Scene> scene;

@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) PM_Field* field;


@end
