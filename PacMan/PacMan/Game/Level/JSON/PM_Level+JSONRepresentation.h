//
//  PM_Level+JSONRepresentation.h
//  PacMan
//
//  Created by Victor Sukochev on 06/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "PM_Level.h"

@interface PM_Level (JSONRepresentation)

+ (PM_Level*)representationWithJSONFile:(NSString*)filePath scene:(id <SE_Scene>)scene;
+ (PM_Level*)representationWithData:(NSData*)jsonData scene:(id <SE_Scene>)scene;

- (id) initWithJSONRepresentationFile:(NSString*)filePath scene:(id <SE_Scene>)scene;
- (id) initWithJSONRepresentationData:(NSData*)jsonData scene:(id <SE_Scene>)scene;

@end
