//
//  PM_GameObjectsGenerator.h
//  PacMan
//
//  Created by Victor Sukochev on 11/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "PM_JsonRepresentationConstants.h"

// Static
#import "PM_Wall.h"
#import "PM_PointToEat.h"

// Dynamic
#import "PM_Ghost.h"
#import "PM_Pacman.h"


typedef enum
{
    PM_GameObjectTypeUnknown = 0,
    PM_GameObjectTypeStatic,
    PM_GameObjectTypeDynamic,
    PM_GameObjectTypePacman,
}PM_GameObjectType;

@interface PM_GameObjectInfo : NSObject

@property (nonatomic, retain) PM_FieldObject* gameObject;
@property (nonatomic, assign) PM_GameObjectType type;

@end


PM_GameObjectInfo* gameObjectForString(NSString* objStr);