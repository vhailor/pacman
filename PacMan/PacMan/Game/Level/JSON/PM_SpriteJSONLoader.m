//
//  PM_SpriteJSONLoader.m
//  PacMan
//
//  Created by Victor Sukochev on 06/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "PM_SpriteJSONLoader.h"

#import "PM_Utility.h"


void setupSrite(id <SE_Sprite> sprite, NSDictionary* dict, NSError** error)
{
    if ( ![dict isKindOfClass:[NSDictionary class]] )
    {
        *error = generateErrorWithDescription(@"Wrong sprite format", @"PM_Game");
    }
    
    NSString* filename = dict[@"filename"];
    if ( ![filename isKindOfClass:[NSString class]] )
    {
        *error = generateErrorWithDescription(@"Wrong sprite filename format", @"PM_Game");
    }
    
    NSArray* frameArray = dict[@"frame"];
    if ( frameArray.count != 4 )
    {
        *error = generateErrorWithDescription(@"Wrong texture coordinates format", @"PM_Game");
    }
    
    sprite.imagePath    = [[NSBundle mainBundle] pathForResource:filename ofType:nil];
    sprite.textureFrame = CGRectMake([(NSNumber*)frameArray[0] intValue],
                                     [(NSNumber*)frameArray[1] intValue],
                                     [(NSNumber*)frameArray[2] intValue],
                                     [(NSNumber*)frameArray[3] intValue]);
}