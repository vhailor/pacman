//
//  PM_GameObjectsGenerator.m
//  PacMan
//
//  Created by Victor Sukochev on 11/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "PM_GameObjectsGenerator.h"


PM_GameObjectInfo* gameObjectForString(NSString* objStr)
{
    PM_GameObjectInfo* gameObject = [PM_GameObjectInfo new];
    
    if ( [objStr isEqualToString:kPmWall1String] ||
         [objStr isEqualToString:kPmWall2String] ||
         [objStr isEqualToString:kPmWall3String] ||
         [objStr isEqualToString:kPmWall4String] ||
         [objStr isEqualToString:kPmWall5String] ||
         [objStr isEqualToString:kPmWall6String] ||
         [objStr isEqualToString:kPmWall7String] )
    {
        gameObject.gameObject = [PM_Wall new];
        gameObject.type       = PM_GameObjectTypeStatic;
        return gameObject;
    }
    
    if ( [objStr isEqualToString:kPmPointToEatString] )
    {
        gameObject.gameObject = [PM_PointToEat new];
        gameObject.type       = PM_GameObjectTypeStatic;
        return gameObject;
    }
    
    if ( [objStr isEqualToString:kPmGhostString] )
    {
        gameObject.gameObject = [PM_Ghost new];
        gameObject.type       = PM_GameObjectTypeDynamic;
        return gameObject;
    }
    
    if ( [objStr isEqualToString:kPmPacmanString] )
    {
        gameObject.gameObject = [PM_Pacman new];
        gameObject.type       = PM_GameObjectTypePacman;
        return gameObject;
    }
    
    return nil;
}

@implementation PM_GameObjectInfo


@end