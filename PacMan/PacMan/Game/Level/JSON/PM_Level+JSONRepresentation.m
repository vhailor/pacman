//
//  PM_Level+JSONRepresentation.m
//  PacMan
//
//  Created by Victor Sukochev on 06/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "PM_Level+JSONRepresentation.h"

#import "PM_Utility.h"
#import "JSONKit.h"

#import "PM_FieldJsonRepresentation.h"

@implementation PM_Level (JSONRepresentation)

#pragma mark - Lifecycle

+ (PM_Level*)representationWithJSONFile:(NSString*)filePath scene:(id<SE_Scene>)scene
{
    return [[PM_Level alloc] initWithJSONRepresentationFile:filePath scene:scene];
}

+ (PM_Level*)representationWithData:(NSData*)jsonData scene:(id<SE_Scene>)scene
{
    return [[PM_Level alloc] initWithJSONRepresentationData:jsonData scene:scene];
}

- (id) initWithJSONRepresentationFile:(NSString*)filePath scene:(id<SE_Scene>)scene
{
    NSError* error = nil;
    NSData* jsonData = [NSData dataWithContentsOfFile:filePath
                                              options:nil
                                                error:&error];
    
    if ( error != nil )
    {
        NSLog(@"Level loading error %@", error.localizedDescription);
        return nil;
    }
    
    if ( jsonData == nil )
    {
        NSLog(@"Empty level file");
        return nil;
    }
    
    self = [self initWithJSONRepresentationData:jsonData scene:scene];
    if ( self != nil )
    {

    }
    
    return self;
}

- (id) initWithJSONRepresentationData:(NSData*)jsonData scene:(id<SE_Scene>)scene
{
    if ( self != nil )
    {
        self.scene = scene;
        
        NSError* error = nil;
        [self generateLevelWithData:jsonData error:&error];
        
        if ( error != nil )
        {
            NSLog(@"Error: %@", error.localizedDescription);
            return nil;
        }
    }
    
    return self;
}

#pragma mark - Private methods

- (void) generateLevelWithData:(NSData*)data error:(NSError**)error
{
    NSMutableString* jsonStr = [[NSMutableString alloc] initWithBytes:data.bytes length:data.length encoding:NSUTF8StringEncoding];
    [self clearMutableJsonString:jsonStr];
    //NSDictionary* jsonDict = [data objectFromJSONDataWithParseOptions:0 error:error];//[data objectFromJSONData];
    NSDictionary*  jsonDict = [jsonStr objectFromJSONStringWithParseOptions:0 error:error];

    if ( *error != nil )
    {
        return;
    }
    
    if ( ![jsonDict isKindOfClass:[NSDictionary class]] )
    {
        *error = generateErrorWithDescription(@"Wrong level file format", @"PM_Game");
        return;
    }
    
    // Level
    NSDictionary* levelDict = jsonDict[@"level"];
    if ( ![levelDict isKindOfClass:[NSDictionary class]] )
    {
        *error = generateErrorWithDescription(@"Wrong level file format", @"PM_Game");
        return;
    }
    
    NSString* fieldStr = levelDict[@"data"];
    
    if ( ![fieldStr isKindOfClass:[NSString class]] )
    {
        *error = generateErrorWithDescription(@"Wrong level data format", @"PM_Game");
        return;
    }
    
    NSString* titleStr = levelDict[@"title"];
    if ( ![titleStr isKindOfClass:[NSString class]] )
    {
        *error = generateErrorWithDescription(@"Wrong level title format", @"PM_Game");
        return;
    }
    self.title = titleStr;
    
    NSArray* fieldSize =  levelDict[@"field_size"];
    if ( fieldSize.count != 2 )
    {
        *error = generateErrorWithDescription(@"Wrong level field size format", @"PM_Game");
        return;
    }
    
    NSArray* frameSize =  levelDict[@"frame_size"];
    if ( frameSize.count != 2 )
    {
        *error = generateErrorWithDescription(@"Wrong level frame size format", @"PM_Game");
        return;
    }
    
    // Sprites
    NSDictionary* spritesDict = jsonDict[@"sprites"];
    if ( ![spritesDict isKindOfClass:[NSDictionary class]] )
    {
        *error = generateErrorWithDescription(@"Wrong sprites format", @"PM_Game");
        return;
    }
    
    self.field = [[PM_FieldJsonRepresentation alloc] initWithScene:self.scene
                                                       fieldString:fieldStr
                                                           spritesInfo:spritesDict
                                                         fieldSize:CGSizeMake([fieldSize[0] intValue],
                                                                              [fieldSize[1] intValue])
                                                         frameSize:CGSizeMake([frameSize[0] intValue],
                                                                              [frameSize[1] intValue])];
         
}

- (void) clearMutableJsonString:(NSMutableString*)mutableStr
{
    [mutableStr replaceOccurrencesOfString:@"\n" withString:@"" options:0 range:NSMakeRange(0, mutableStr.length)];
    [mutableStr replaceOccurrencesOfString:@"\t" withString:@"" options:0 range:NSMakeRange(0, mutableStr.length)];
    [mutableStr replaceOccurrencesOfString:@" "  withString:@"" options:0 range:NSMakeRange(0, mutableStr.length)];
}

@end
