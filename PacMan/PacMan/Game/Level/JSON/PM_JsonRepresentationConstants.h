//
//  PM_JsonRepresentationConstants.h
//  PacMan
//
//  Created by Victor Sukochev on 11/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import <Foundation/Foundation.h>

/*! Constants for object generator
*/

extern NSString* const kPmWall1String;
extern NSString* const kPmWall2String;
extern NSString* const kPmWall3String;
extern NSString* const kPmWall4String;
extern NSString* const kPmWall5String;
extern NSString* const kPmWall6String;
extern NSString* const kPmWall7String;

extern NSString* const kPmPointToEatString;
extern NSString* const kPmPacmanString;
extern NSString* const kPmGhostString;

