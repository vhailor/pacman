//
//  PM_FieldJsonRepresentation.m
//  PacMan
//
//  Created by Victor Sukochev on 07/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "PM_FieldJsonRepresentation.h"

#import "PM_SpriteJSONLoader.h"
#import "PM_GameObjectsGenerator.h"
#import "PM_Emptiness.h"


@implementation PM_FieldJsonRepresentation
{
    @private
    NSDictionary* _spritesInfo;
    id <SE_Scene> _scene;
}

#pragma mark - Public methods

- (id) initWithScene:(id<SE_Scene>)scene
         fieldString:(NSString*)fieldStr
         spritesInfo:(NSDictionary*)spritesInfo
           fieldSize:(CGSize)fieldSize
           frameSize:(CGSize)frameSize
{
    self = [self initWithScene:scene];
    
    if ( self == nil )
    {
        return nil;
    }
    
    _spritesInfo = spritesInfo;
    _scene       = scene;
    self.dynamicObjects = [NSMutableArray array];
    self.staticObjects  = [NSMutableArray array];
    self.fieldSize      = fieldSize;
    self.frameSize      = frameSize;
    
    NSMutableString* clearedStr = [fieldStr mutableCopy];
    [self clearMutableStringWithFieldData:clearedStr];
    
    CGFloat dx = frameSize.width  / fieldSize.width;
    CGFloat dy = frameSize.height / fieldSize.height;
    
    for ( NSUInteger y = 0; y < fieldSize.height; y++ )
    {
        for (NSUInteger x = 0; x < fieldSize.width; x++)
        {
            NSString* symbolStr = [clearedStr substringWithRange:NSMakeRange( x + y * fieldSize.width, 1)];
            id <SE_Sprite> sprite = [self spriteForFieldString:symbolStr];
            
            if ( sprite == nil )
            {
                continue;
            }
            
            sprite.frame = CGRectMake(x * dx, y * dy, dx, dy);
            [_scene addSprite:sprite];
            
            PM_GameObjectInfo* gameObjectInfo = gameObjectForString(symbolStr);
            gameObjectInfo.gameObject.fieldPosition = CGPointMake(x, y);
            [self saveGameObject:gameObjectInfo andAttachSprite:sprite];
            
            //TODO: Refactor. Not proper way to count points
            if ( [symbolStr isEqualToString:@"s"] )
            {
                self.pointsToEatLeft++;
            }
        }
    }
    
    return self;
}

#pragma mark - Private methods

- (void) clearMutableStringWithFieldData:(NSMutableString*)mutableStr
{
    [mutableStr replaceOccurrencesOfString:@"\n" withString:@"" options:0 range:NSMakeRange(0, mutableStr.length)];
    [mutableStr replaceOccurrencesOfString:@"\t" withString:@"" options:0 range:NSMakeRange(0, mutableStr.length)];
    [mutableStr replaceOccurrencesOfString:@" "  withString:@"" options:0 range:NSMakeRange(0, mutableStr.length)];
}

- ( id <SE_Sprite> ) spriteForFieldString:(NSString*)fieldStr
{
    NSDictionary* spriteInfo = _spritesInfo[fieldStr];
    
    if ( spriteInfo == nil )
    {
        return nil;
    }
    
    id <SE_Sprite> sprite = [_scene defaultSprite];
    NSError* error = nil;
    setupSrite(sprite, spriteInfo, &error);
    
    if ( error != nil )
    {
        NSLog(@"Error %@", error.localizedDescription);
        return nil;
    }
    
    return sprite;
}

- (void) saveGameObject:(PM_GameObjectInfo*)gameObjectInfo andAttachSprite:(id <SE_Sprite>)sprite
{
    id generatedObject;
    switch (gameObjectInfo.type)
    {
        case PM_GameObjectTypeUnknown:
            //Do nothing
            break;
            
        case PM_GameObjectTypeStatic:
            generatedObject = gameObjectInfo.gameObject;
            sprite.z = 0;
            ((PM_StaticGameObject*)generatedObject).sprite = sprite;
            [self.staticObjects addObject:generatedObject];
            break;
            
        case PM_GameObjectTypeDynamic:
            generatedObject = gameObjectInfo.gameObject;
            sprite.z = -1;
            ((PM_DynamicGameObject*)generatedObject).sprite = sprite;
            [self.dynamicObjects addObject:generatedObject];
            [self.staticObjects  addObject:[PM_Emptiness new]];
            break;
            
        case PM_GameObjectTypePacman:
            generatedObject = gameObjectInfo.gameObject;
            sprite.z = -1;
            ((PM_Pacman*)generatedObject).sprite = sprite;
            [self.dynamicObjects addObject:generatedObject];
            [self.staticObjects  addObject:[PM_Emptiness new]];
            self.pacman = generatedObject;
            break;
            
        default:
            NSAssert(0, @"Wrong game object type");
            break;
    }
}

@end
