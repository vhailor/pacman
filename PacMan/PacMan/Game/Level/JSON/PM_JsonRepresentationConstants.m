//
//  PM_JsonRepresentationConstants.m
//  PacMan
//
//  Created by Victor Sukochev on 11/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "PM_JsonRepresentationConstants.h"

NSString* const kPmWall1String = @"t";
NSString* const kPmWall2String = @"v";
NSString* const kPmWall3String = @"b";
NSString* const kPmWall4String = @"l";
NSString* const kPmWall5String = @"h";
NSString* const kPmWall6String = @"r";
NSString* const kPmWall7String = @"T";

NSString* const kPmPointToEatString = @"s";
NSString* const kPmPacmanString     = @"p";
NSString* const kPmGhostString      = @"g";