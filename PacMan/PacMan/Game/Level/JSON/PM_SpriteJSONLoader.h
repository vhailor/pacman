//
//  PM_SpriteJSONLoader.h
//  PacMan
//
//  Created by Victor Sukochev on 06/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "SE_Sprite.h"

void setupSrite(id <SE_Sprite> sprite, NSDictionary* dict, NSError** error);
