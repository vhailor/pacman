//
//  PM_FieldJsonRepresentation.h
//  PacMan
//
//  Created by Victor Sukochev on 07/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "PM_Field.h"

@interface PM_FieldJsonRepresentation : PM_Field

- (id) initWithScene:(id<SE_Scene>)scene
         fieldString:(NSString*)fieldStr
         spritesInfo:(NSDictionary*)spritesInfo
           fieldSize:(CGSize)fieldSize
           frameSize:(CGSize)frameSize;

@end
