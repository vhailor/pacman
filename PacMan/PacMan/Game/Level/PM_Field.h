//
//  PM_Field.h
//  PacMan
//
//  Created by Victor Sukochev on 06/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SE_Scene.h"
#import "PM_Pacman.h"

@interface PM_Field : NSObject

@property (nonatomic, strong) id <SE_Scene> scene;
@property (nonatomic, strong) NSMutableArray* staticObjects;
@property (nonatomic, strong) NSMutableArray* dynamicObjects;
@property (nonatomic, assign) CGSize fieldSize;
@property (nonatomic, assign) CGSize frameSize;
@property (nonatomic, strong) PM_Pacman* pacman;

@property (nonatomic, assign) NSUInteger pointsToEatLeft;


- (id) initWithScene:(id <SE_Scene>)scene;

@end
