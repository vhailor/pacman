//
//  PM_Field.m
//  PacMan
//
//  Created by Victor Sukochev on 06/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "PM_Field.h"

@implementation PM_Field


- (id) initWithScene:(id <SE_Scene>)scene
{
    self = [super init];
    
    if ( self != nil )
    {
        _scene = scene;
    }
    
    return self;
}

@end
