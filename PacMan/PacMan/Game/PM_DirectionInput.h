//
//  PM_DirectionInput.h
//  PacMan
//
//  Created by Victor Sukochev on 16/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    MovementDirectionUnknown = 0,
    MovementDirectionUp,
    MovementDirectionDown,
    MovementDirectionLeft,
    MovementDirectionRight,
}MovementDirection;

@protocol PM_DirectionInput <NSObject>

- (MovementDirection) getCurrentDirection;
- (void) reset;
 

@end
