//
//  PM_ViewController.h
//  PacMan
//
//  Created by Victor Sukochev on 29/01/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PM_SpriteEngineOpenGL.h"
#import "SEOpenGL_Sprite.h"
#import "SEOpenGL_Scene.h"
#import "PM_Game.h"

@interface PM_SpriteEngineOpenGLViewController : GLKViewController <PM_GameDelegate, UIAlertViewDelegate>

@property (nonatomic, strong) PM_SpriteEngineOpenGL* engine;

@end
