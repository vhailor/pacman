//
//  PM_Utility.h
//  PacMan
//
//  Created by Victor Sukochev on 06/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import <Foundation/Foundation.h>

NSError* generateErrorWithDescription(NSString* description, NSString* domain);