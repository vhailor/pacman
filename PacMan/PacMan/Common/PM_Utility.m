//
//  PM_Utility.m
//  PacMan
//
//  Created by Victor Sukochev on 06/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "PM_Utility.h"

NSError* generateErrorWithDescription(NSString* description, NSString* domain)
{
    NSMutableDictionary *errorDetails = [NSMutableDictionary dictionary];
    [errorDetails setValue:description
                    forKey:NSLocalizedDescriptionKey];
    
    NSError* error = [NSError errorWithDomain:domain
                                         code:1
                                     userInfo:errorDetails];
    
    return error;
}