//
//  main.m
//  PacMan
//
//  Created by Victor Sukochev on 29/01/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PM_AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PM_AppDelegate class]));
    }
}
