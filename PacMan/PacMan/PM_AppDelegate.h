//
//  PM_AppDelegate.h
//  PacMan
//
//  Created by Victor Sukochev on 29/01/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PM_SpriteEngineOpenGLViewController;

@interface PM_AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) PM_SpriteEngineOpenGLViewController *viewController;

@end
