//
//  PM_ViewController.m
//  PacMan
//
//  Created by Victor Sukochev on 29/01/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "PM_SpriteEngineOpenGLViewController.h"

#import "PM_Level+JSONRepresentation.h"
#import "PM_InputView.h"


@interface PM_SpriteEngineOpenGLViewController ()
{
    @private
    IBOutlet PM_InputView* _inputView;
    IBOutlet UILabel* _pointsLabel;
}
@property (strong, nonatomic) EAGLContext *context;
@property (strong, nonatomic) GLKBaseEffect *effect;

- (void)setupGL;
- (void)tearDownGL;

@end

@implementation PM_SpriteEngineOpenGLViewController
{
    @private
    PM_Game* _game;
    NSDate*  _lastUpdateDate;
}

#pragma mark - Public properties
- (void) setEngine:(PM_SpriteEngineOpenGL *)engine
{
    _engine = engine;
    engine.context = _context;
}
#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];

    if (!self.context) {
        NSLog(@"Failed to create ES context");
    }
    
    GLKView *view = (GLKView *)self.view;
    view.context = self.context;
    view.drawableDepthFormat = GLKViewDrawableDepthFormat24;
    
    _game = [PM_Game new];
    _game.directionInput = _inputView;
    _game.gameDelegate = self;
    
    [self setupGL];
    
}

- (void)dealloc
{    
    [self tearDownGL];
    
    if ([EAGLContext currentContext] == self.context) {
        [EAGLContext setCurrentContext:nil];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

    if ([self isViewLoaded] && ([[self view] window] == nil)) {
        self.view = nil;
        
        [self tearDownGL];
        
        if ([EAGLContext currentContext] == self.context) {
            [EAGLContext setCurrentContext:nil];
        }
        self.context = nil;
    }

    // Dispose of any resources that can be recreated.
}

- (void)setupGL
{
    [EAGLContext setCurrentContext:self.context];    

    _engine.context = _context;

    [self initializeLevel];
    
    glEnable(GL_DEPTH_TEST);

}

- (void)tearDownGL
{
    [EAGLContext setCurrentContext:self.context];

}

- (void) initializeLevel
{
    SEOpenGL_Scene* scene = [SEOpenGL_Scene new];
    
    NSString* levelPath = [[NSBundle mainBundle] pathForResource:@"level" ofType:@"json"];
    _game.activeLevel   = [[PM_Level alloc] initWithJSONRepresentationFile:levelPath scene:scene];
    
    [_engine loadScene:scene withProgressBlock:^(CGFloat progress) {
        NSLog(@"Loadig progress: %d", (int)progress * 100);
    } completionBlock:^(NSError* error) {
        if ( error != nil )
        {
            NSLog(@"Loading error %@:", error.localizedDescription);
        }
    }];
    
    [_engine present:scene atFrame:self.view.bounds];
    [_inputView reset];
    
    _pointsLabel.text = [NSString stringWithFormat:@"%d", _game.activeLevel.field.pointsToEatLeft];
}

#pragma mark - GLKView and GLKViewController delegate methods

- (void)update
{
    if ( _lastUpdateDate == nil )
    {
        _lastUpdateDate = [NSDate date];
    }
    
    NSTimeInterval timeIntervalSinceLastUpdate = [[NSDate date] timeIntervalSinceDate:_lastUpdateDate];
    _lastUpdateDate = [NSDate date];
    
    [_game update:timeIntervalSinceLastUpdate];
}

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);    
  
    [_engine render];

}

#pragma mark - PM_GameDelegate methods

- (void) gameDidWin:(PM_Game *)game
{
    UIAlertView* wonAlertView = [[UIAlertView alloc] initWithTitle:@"You Won!"
                                                            message:nil
                                                           delegate:self
                                                  cancelButtonTitle:@"Try Again?"
                                                  otherButtonTitles:nil];
    [wonAlertView show];
}

- (void) gameDidLost:(PM_Game *)game
{
    UIAlertView* lostAlertView = [[UIAlertView alloc] initWithTitle:@"You Lost"
                                                            message:@"Ghost captured you"
                                                           delegate:self
                                                  cancelButtonTitle:@"Try Again?"
                                                  otherButtonTitles:nil];
    [lostAlertView show];
}

- (void) game:(PM_Game*)game didPointsLefyUpdate:(NSUInteger)pointsLeft
{
     _pointsLabel.text = [NSString stringWithFormat:@"%d", pointsLeft];
}

#pragma mark - UIAlertViewDelegate methods

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ( buttonIndex == 0 )
    {
        [self initializeLevel];
    }
}


@end
