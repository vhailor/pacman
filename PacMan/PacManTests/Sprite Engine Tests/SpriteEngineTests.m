//
//  SpriteEngineTests.m
//  PacMan
//
//  Created by Victor Sukochev on 02/02/2013.
//  Copyright (c) 2013 Victor Sukochev. All rights reserved.
//

#import "SpriteEngineTests.h"

#import "PM_SpriteEngineOpenGL.h"
#import "SEOpenGL_Sprite.h"
#import "SEOpenGL_Scene.h"

@implementation SpriteEngineTests
{
    PM_SpriteEngineOpenGL* _engine;
    
    SEOpenGL_Sprite* _spite1;
    SEOpenGL_Sprite* _spite2;
    SEOpenGL_Scene*  _scene;
    
}

- (void) setUp
{
    NSString* imagePath = [[NSBundle bundleForClass:[self class]] pathForResource:@"test_sprites" ofType:@"png"];
       
    _spite1 = [SEOpenGL_Sprite new];
    _spite1.imagePath = imagePath;
    
    _spite2 = [SEOpenGL_Sprite new];
    _spite2.imagePath = imagePath;
    
    _scene = [SEOpenGL_Scene new];
    [_scene addSprite:_spite1];
    [_scene addSprite:_spite2];
    
    _engine = [PM_SpriteEngineOpenGL new];   
}

- (void) tearDown
{
    
}

- (void) testTextureLoading
{
    NSLog(@"================= Loading textures ======================");
    __block BOOL isLoading = YES;
    [_engine loadScene:_scene withProgressBlock:^(CGFloat progress) {
        NSLog(@"Loadig progress: %d", (int)progress * 100);
    } completionBlock:^(NSError* error) {        
        if ( error != nil )
        {
            NSLog(@"Loading error %@:", error.localizedDescription);
        }
        isLoading = NO;
    }];
    
    
    while ( isLoading )
    {
        [NSThread sleepForTimeInterval:1.0f];
    }
    
    STAssertTrue(1, @"Texture loading test passed");
}

@end
